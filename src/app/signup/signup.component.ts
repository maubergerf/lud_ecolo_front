import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  private pseudo = '';
  private password = '';
  private passwordConfirm = '';

  private pseudoError = false;
  private differentPasswords = false;
  private invalidPseudo = false;
  private passwordTooShort = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  signup() {
    this.pseudoError = false;
    this.differentPasswords = false;
    this.invalidPseudo = false;
    this.passwordTooShort = false;

    if (/^[a-z0-9]+$/i.test(this.pseudo)) {
      if (this.password.length < 6 || this.passwordConfirm.length < 6) {
        // Mot de passe trop court
        this.passwordTooShort = true;
      } else {
        if (this.password === this.passwordConfirm) {
          this.authService.signup(this.pseudo, this.password, error => {
            // Pseudo déjà pris
            console.error(error);
            this.pseudoError = true;
          });
        } else {
          // Mots de passes différents
          console.log('mots de passe différents');
          this.differentPasswords = true;
        }
      }
    } else {
      // Pseudo invalide
      console.log('pseudo invalide');
      this.invalidPseudo = true;
    }
  }
}
