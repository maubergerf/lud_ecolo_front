import { SignupComponent } from './signup/signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { DocumentationComponent } from './documentation/documentation.component';
import { Jeu1Component } from './jeu1/jeu1.component';
import { LoginChoiceComponent } from './login-choice/login-choice.component';
import { AuthGuard } from './auth/auth.guard';
import { GuestGuard } from './auth/guest.guard';
import { UserComponent } from './user/user.component';


const routes: Routes = [
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: '',
    component: DocumentationComponent
  },
  {
    path: 'jeu-origine',
    component: Jeu1Component,
    canActivate: [AuthGuard]
  },
  {
    path: 'login-choice',
    component: LoginChoiceComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'profile',
    component: UserComponent,
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
