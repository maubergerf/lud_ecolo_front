import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  private pseudo: string;
  private password: string;

  private wrongPassword = false;

  constructor(private httpClient: HttpClient, private authService: AuthService) { }

  ngOnInit() {
  }

  signin() {
    this.wrongPassword = false;
    this.authService.signin(this.pseudo, this.password, error => {
      console.log(error);
      this.wrongPassword = true;
    });
  }
}
