import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/index';
import { AvatarService } from '../avatar/avatar.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private signinPath = 'http://localhost:3000/user/signin';
  private signupPath = 'http://localhost:3000/user/signup';
  private userPath = 'http://localhost:3000/user';

  public attemptedPath: string = undefined;

  private user: any;
  public userData: Subject<any>;

  public getToken() {
    return localStorage.getItem('ludecolo-token');
  }

  public disconnect() {
    localStorage.removeItem('ludecolo-token');
    this.user = undefined;
    this.userData.next(this.user);
  }

  public getImage() {
    if (this.user) {
      return this.user.image;
    } else {
      return "";
    }
  }

  public getUser() {
    return this.user;
  }

  public isConnected() {
    return this.getToken() !== null;
  }

  public signin(pseudo, password, errorCallback) {
    this.httpClient.post(this.signinPath, { pseudo: pseudo, password: password }, {}).subscribe((data: any) => {
      localStorage.setItem('ludecolo-token', data.token);
      this.user = data.user;
      this.userData.next(this.user);
      if (this.attemptedPath !== undefined) {
        this.router.navigate([this.attemptedPath]);
        this.attemptedPath = undefined;
      } else {
        this.router.navigate(['']);
      }
    }, error => {
      this.disconnect();
      errorCallback(error);
    })
  }

  public signup(pseudo, password, errorCallback) {
    this.httpClient.post(this.signupPath, { pseudo: pseudo, password: password, avatar: this.avatarService.getRandomAvatar().path }, {}).subscribe((data: string) => {
      this.signin(pseudo, password, errorCallback)
      }, error => {errorCallback(error);
    });
  }

  public edit(user, successCallback, errorCallback) {
    let headers = new HttpHeaders().set("Authorization", "Bearer " + this.getToken());
    this.httpClient.put(this.userPath, user, { headers:headers }).subscribe(user => {
      this.user = user;
      this.userData.next(user);
      successCallback();
    }, errorCallback);
  }

  constructor(private httpClient: HttpClient, private router: Router, private avatarService: AvatarService) {
    this.userData = new Subject<any>();
    if (this.isConnected()) {
      this.httpClient.get(this.userPath, {headers: new HttpHeaders().set("Authorization", "Bearer " + this.getToken() )}).subscribe(user => {
        this.user = user;
        this.userData.next(user);
      })
    }
  }
}
