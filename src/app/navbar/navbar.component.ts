import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs/index';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  private user: any;
  private userSubscription: Subscription;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.userSubscription = this.authService.userData.subscribe(user => {
      this.user = user;
    })
  }

}
