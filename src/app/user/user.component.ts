import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/index';
import { AuthService } from '../auth/auth.service';
import { AvatarService } from '../avatar/avatar.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public user: any;
  private userSubscription: Subscription;
  private avatars = [];
  private scores = [];

  private passwordTooShort = false;
  private modified = false;

  private scorePath = 'http://localhost:3000/user/scores';

  public password = "";

  constructor(private authService: AuthService, private avatarService: AvatarService, private router: Router, private httpClient: HttpClient) {
    this.user = {};
  }

  ngOnInit() {
    this.avatars = this.avatarService.getAvatars();
    this.avatars.sort((a, b) => a.name.localeCompare(b.name));
    this.userSubscription = this.authService.userData.subscribe(user => {
      this.user = user;
    });
    this.user = this.authService.getUser();
    this.getScores();
  }

  edit() {
    this.modified = false;
    if (this.password.length < 6) {
      this.passwordTooShort = true;
    } else {
      this.passwordTooShort = false;
      this.user.password = this.password;
      this.authService.edit(this.user, () => { this.modified = true; }, error => console.error(error));
    }
  }

  getScores() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.authService.getToken());
    this.httpClient.get(this.scorePath, {headers: headers}).subscribe((scores: [any]) => {
      this.scores = scores.reverse();
    });
  }

  getLastScores(n) {
    return this.scores.slice(0, n);
  }

  disconnect() {
    this.authService.disconnect();
    this.router.navigate(['']);
  }
}

