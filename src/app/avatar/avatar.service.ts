import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AvatarService {
  private avatars = [
    {
      path: 'assets/img/animals/anteater.svg',
      name: 'Fourmilier'
    },
    {
      path: 'assets/img/animals/bear.svg',
      name: 'Ours'
    },
    {
      path: 'assets/img/animals/beaver.svg',
      name: 'Castor'
    },
    {
      path: 'assets/img/animals/boar.svg',
      name: 'Sanglier'
    },
    {
      path: 'assets/img/animals/buffalo.svg',
      name: 'Buffle'
    },
    {
      path: 'assets/img/animals/cat.svg',
      name: 'Chat'
    },
    {
      path: 'assets/img/animals/chicken.svg',
      name: 'Poussin'
    },
    {
      path: 'assets/img/animals/cow.svg',
      name: 'Vache'
    },
    {
      path: 'assets/img/animals/crow.svg',
      name: 'Corbeau'
    },
    {
      path: 'assets/img/animals/dog.svg',
      name: 'Chien'
    },
    {
      path: 'assets/img/animals/donkey.svg',
      name: 'Âne'
    },
    {
      path: 'assets/img/animals/elephant.svg',
      name: 'Éléphant'
    },
    {
      path: 'assets/img/animals/fox.svg',
      name: 'Renard'
    },
    {
      path: 'assets/img/animals/giraffe.svg',
      name: 'Giraffe'
    },
    {
      path: 'assets/img/animals/hedgehog.svg',
      name: 'Hérisson'
    },
    {
      path: 'assets/img/animals/hen.svg',
      name: 'Poule'
    },
    {
      path: 'assets/img/animals/hippopotamus.svg',
      name: 'Hippopotame'
    },
    {
      path: 'assets/img/animals/horse.svg',
      name: 'Cheval'
    },
    {
      path: 'assets/img/animals/kangaroo.svg',
      name: 'Kangourou'
    },
    {
      path: 'assets/img/animals/koala.svg',
      name: 'Koala'
    },
    {
      path: 'assets/img/animals/leopard.svg',
      name: 'Léopard'
    },
    {
      path: 'assets/img/animals/lion.svg',
      name: 'Lion'
    },
    {
      path: 'assets/img/animals/marten.svg',
      name: 'Martre'
    },
    {
      path: 'assets/img/animals/monkey.svg',
      name: 'Singe'
    },
    {
      path: 'assets/img/animals/mouse.svg',
      name: 'Souris'
    },
    {
      path: 'assets/img/animals/octopus.svg',
      name: 'Poulpe'
    },
    {
      path: 'assets/img/animals/ostrich.svg',
      name: 'Autruche'
    },
    {
      path: 'assets/img/animals/owl.svg',
      name: 'Hibou'
    },
    {
      path: 'assets/img/animals/panda.svg',
      name: 'Panda'
    },
    {
      path: 'assets/img/animals/parrot.svg',
      name: 'Perroquet'
    },
    {
      path: 'assets/img/animals/penguin.svg',
      name: 'Pingouin'
    },
    {
      path: 'assets/img/animals/pig.svg',
      name: 'Cochon'
    },
    {
      path: 'assets/img/animals/polar-bear.svg',
      name: 'Ours polaire'
    },
    {
      path: 'assets/img/animals/rabbit.svg',
      name: 'Lapin'
    },
    {
      path: 'assets/img/animals/racoon.svg',
      name: 'Raton laveur'
    },
    {
      path: 'assets/img/animals/rhinoceros.svg',
      name: 'Rhinocéros'
    },
    {
      path: 'assets/img/animals/rooster.svg',
      name: 'Coq'
    },
    {
      path: 'assets/img/animals/seagull.svg',
      name: 'Mouette'
    },
    {
      path: 'assets/img/animals/seal.svg',
      name: 'Morse'
    },
    {
      path: 'assets/img/animals/sheep.svg',
      name: 'Mouton'
    },
    {
      path: 'assets/img/animals/sloth.svg',
      name: 'Paresseux'
    },
    {
      path: 'assets/img/animals/snake.svg',
      name: 'Serpent'
    },
    {
      path: 'assets/img/animals/tiger.svg',
      name: 'Tigre'
    },
    {
      path: 'assets/img/animals/whale.svg',
      name: 'Baleine'
    },
    {
      path: 'assets/img/animals/zebra.svg',
      name: 'Zèbre'
    }
  ];

  public getAvatars() {
    return this.avatars;
  }

  public getRandomAvatar() {
    return this.avatars[Math.floor(Math.random() * this.avatars.length)]
  }

  constructor() { }
}
