import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-jeu1',
  templateUrl: './jeu1.component.html',
  styleUrls: ['./jeu1.component.scss']
})
export class Jeu1Component implements OnInit {

  private path = 'http://localhost:3000/origine/question';
  private pathScore = 'http://localhost:3000/origine/score';
  public round = 0;
  public score = 0;
  private headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('ludecolo-token'));

  private question: any = undefined;
  private answers = undefined;

  private showHint = false;
  private goodAnswer = false;
  private badAnswer = false;
  private scoreLogged = false;

  constructor(private httpClient: HttpClient, private router: Router) { }

  ngOnInit() {
    
  }

  start() {
    this.round = 0;
    this.getQuestion();
  }

  private shuffleArray(array) {
    var m = array.length, t, i;

    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }

    return array;
  }

  getQuestion() {
    this.round += 1;
    this.goodAnswer = false;
    this.badAnswer = false;

    if (this.round <= 10) {
      this.httpClient.get(this.path, {headers: this.headers})
      .subscribe((data: any) => {
        this.question = data;

        this.answers = [];
        this.answers.push(data.goodAnswer);
        data.badAnswers.forEach(b => {
          this.answers.push(b);
        })

        this.answers = this.shuffleArray(this.answers);
      });
    } else {
      this.scoreLogged = true;
      this.httpClient.post(this.pathScore, { score: this.score }, {headers: this.headers})
        .subscribe(() => {
          console.log("Finish Game");
        }, error => {
          console.error(error);
          this.scoreLogged = false;
        });
    }
  }

  answerQuestion(index) {
    if (this.goodAnswer || this.badAnswer || this.scoreLogged) {
      return;
    }
    
    if (this.round > 10 && !this.scoreLogged) {

    } else {
      if (this.answers[index].name === this.question.goodAnswer.name) {
        this.score+= 1;
        this.goodAnswer = true;
      } else {
        this.badAnswer = true;
      }
    }
  }

  finishGame() {
    this.router.navigate(['']);
  }

  hint() {
    //this.showHint = true;
    this.showHint = this.showHint === true ? false : true;
  }
}
